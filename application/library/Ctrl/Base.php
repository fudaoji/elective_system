<?php

/**
 * 控制器 基础类
 * @author 傅道集
 */
abstract class Ctrl_Base extends Yaf_Controller_Abstract{
	public     $parameters;
	public     $app_config;
	public     $excelExportFile;
	protected  $_userInfo;
	public     $userInfo=array();

	/**
	 * @desc 初始化必要操作
	 */
	public function init(){
		session_start();
		$this->app_config = Yaf_Registry::get("config")->application;
		$this->loadParameters();
		$this->excelFile = $this->app_config->directory."/library/PHPExcel.php";
	}

	/**
	 * @desc 加载常用变量
	 */
	public function loadParameters(){
		if(!$this->parameters){
			include $this->app_config->directory."/common/parameters.php";
			$this->parameters = $parameters;
		}
	}

	/**
	 * @desc 引用excel导出模块
	 */
	public function inputExcelClass(){
		include $this->app_config->directory."/library/PHPExcel_export.php";
	}

	/**
	 * @desc 验证用户登录状态
	 */
	public function auth(){
		if (!isset($_SESSION['code']) && empty($_SESSION['code'])) {
			header("Location: ".SITE_ROOT."login");
			exit();
		}
		$this->userInfo = array('code'      => $_SESSION['code'],
					  			'userName' => $_SESSION['userName'],
					  			'roleName' => $this->parameters[$_SESSION['mid']] );
		return $this->userInfo;
	}

	/**
	 * 注册变量到模板
	 * @param string|array $pKey 要注册的变量或变量数组
	 * @param mixed $pVal 变量对应的值
	 */
	public function assign($pKey, $pVal=''){
		if(is_array($pKey)){
			return $this->_view->assign($pKey);
		}
		$this->_view->assign($pKey, $pVal);
	}

	/**
     * AJAX返回
     * @param string $pMsg 提示信息
     * @param int $pStatus 返回状态
     * @param mixed $pData 要返回的数据
     * @param string $pStatus ajax返回类型
     */
	protected function ajax($pStatus=1, $pMsg=null, $pData=array(), $pType='json'){
		# 编码
		header("Content-Type:text/html; charset=utf-8");
		# 信息
		$tResult = array('status'=>$pStatus, 'info'=>$pMsg, 'data'=>$pData);
		# 格式
		'json' == $pType && exit(json_encode($tResult));
		'xml' == $pType && exit(xml_encode($tResult));
		'eval' == $pType && exit($pData);
	}


	/**
     * @desc 获取$_GET全局变量数组的某参数值,并进行转义等处理，提升代码安全.注:参数支持数组
     * @param string $string    所要获取$_GET的参数
     * @param string $default_param 默认参数, 注:只有$string不为数组时有效
     * @return string    $_GET数组某参数值
     */
	public function get($string, $default_param = null) {

        $param = $this->getRequest()->getParam($string, $default_param);
		$param = is_null($param) ? '' : (!is_array($param) ? trim($param) : $param);
        return $param;
    }

    /**
     * @desc 获取$_POST全局变量数组的某参数值,并进行转义等处理，提升代码安全.注:参数支持数组
     * @param string $string    所要获取$_POST的参数
     * @param string $default_param 默认参数, 注:只有$string不为数组时有效
     * @return string    $_POST数组某参数值
     */
    public function post($string, $default_param = null) {

       $param = $this->getRequest()->getPost($string, $default_param);
       $param = is_null($param) ? '' : (!is_array($param) ? trim($param) : $param);
       return $param;
    }

}