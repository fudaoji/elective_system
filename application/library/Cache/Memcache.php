<?php
/**
 * @desc MemCache 缓存
 */
class Cache_Memcache {
	private static $mem;

	/**
	 * @desc 构造函数
	 */
	public function __destruct() {
		self::$mem -> close();
	}

	/**
	 * 单例接口
	 * @param string $pConfig 服务器配置
	 * @return Memcache
	 */
	static function & instance($pConfig = 'session') {
		if (!isset(self::$mem[$pConfig])) {
			//配置
			$tMC = Yaf_Registry::get("config") -> mc -> $pConfig -> toArray();
			//连接
			self::$mem[$pConfig] = new Memcache();
			self::$mem[$pConfig] -> pconnect($tMC['host'], $tMC['port']) or self::$mem[$pConfig] = null;
		}
		return self::$mem[$pConfig];
	}

	/**
	 * @desc 取缓存
	 * @param string|array  $pKey  要获取值的key或key数组
	 * @return array|string 返回获取值
	 */
	static function get($pKey, $pField = '') {
		if (!$pField)
			return Cache_Memcache::instance() -> get($pKey);
		if ($tData = Cache_Memcache::instance() -> get($pKey)) {
			if (isset($tData[$pField]))
				return $tData[$pField];
		}
		return false;
	}

	/**
	 * @desc 添加缓存
	 * @param  string|array $pKey 要设置的key或key数组
	 * @param string $pVal 要缓存内容
	 * @param int $pExp 缓存保存时间 
	 * @return bool
	 */
	static function set($pKey, $pVal, $pExp = 3600) {
		return Cache_Memcache::instance() -> set($pKey, $pVal, $pExp);
	}

}
