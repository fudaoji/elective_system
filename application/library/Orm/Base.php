<?php
class Orm_Base{
	/**
	 * @desc 数据库链接
	 * @var obj
	 */
	protected $db;

	/**
	 * @desc查询参数
	 * @var array
	 */
	public $options = array();

	/**
	 * @desc PDO 实例化对象
	 * @var object
	 */
	static $instance = array();

	/**
	 * @desc 配置
	 * @var string
	 */
	protected $_config;

	/**
	 * @desc错误信息
	 */
	public $error = array();

	/**
	 * @desc 构造函数
	 * @param int $pPK 查询的索引
	 * @param string $pConfig 启用的数据库配置信息
	 */
	function __construct($pPK = 0, $pConfig = 'default'){
		$this->_config = $pConfig;   //联系到配置文件中db.default
		// 通过主键取出数据
		if($pPK && $pPK = abs($pPK)){
			if($tRow = $this->fRow($pPK)){
				foreach($tRow as $k1 => $v1) $this->$k1 = $v1;
			}
			else{
				foreach($this->field as $k1 => $v1) $this->$k1 = false;
			}
		}
	}

	/**
	 * @desc 特殊方法实现
	 * @param string $pMethod 特殊方法名
	 * @param array $pArgs 传递的参数
	 * @return mixed
	 */
	public function __call($pMethod, $pArgs){
		# 连贯操作的实现
		if(in_array($pMethod, array('field', 'table', 'where', 'order', 'limit', 'page', 'having', 'group', 'lock', 'distinct'), true)){
			$this->options[$pMethod] = $pArgs[0];
			return $this;
		}
		# 统计查询的实现
		if(in_array($pMethod, array('count', 'sum', 'min', 'max', 'avg'))){
			$field = isset($pArgs[0])? $pArgs[0]: '*';
			return $this->fOne("$pMethod($field)");
		}
		# 根据某个字段获取记录
		if('ff' == substr($pMethod, 0, 2)){
			return $this->where(strtolower(substr($pMethod, 2)) . "='{$pArgs[0]}'")->fRow();
		}
	}

	/**
	 * @desc 数据库连接
	 * @param string $pConfig 数据库配置信息
	 */
	static function &instance($pConfig = 'default'){
		if(empty(self::$instance[$pConfig])){
			# 实例化PDO,将配置文件中的db.default格式化为数组
			$tDB = Yaf_Registry::get("config")->db->$pConfig->toArray(); 
			self::$instance[$pConfig] = @new PDO($tDB['dsn'], $tDB['username'], $tDB['password'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		}
		return self::$instance[$pConfig];  //返回数据库连接句柄
	}

	/**
	 *@desc 过滤危险数据
	 * @param array $pData 要过滤的键值对
	 */
	private function _filter(&$pData){
		foreach($pData as $k1 => &$v1){
			if(empty($this->field[$k1])){
				unset($pData[$k1]);
				continue;
			}
			//将v1值中的\\省略掉，将'用\'替换
			$v1 = strtr($v1, array('\\' => '', "'" => "\'"));
		}
		return $pData? true: false;
	}

	/**
	 * @desc 查询参数
	 * @param mixed $pOpt 查询条件
	 */
	private function _options($pOpt = array()){
		# 合并查询条件
		$tOpt = $pOpt? array_merge($this->options, $pOpt): $this->options;
		$this->options = array();
		# 数据表
		empty($tOpt['table']) && $tOpt['table'] = $this->table;
		empty($tOpt['field']) && $tOpt['field'] = '*';
		#  查询条件
		if(isset($tOpt['where']) && is_array($tOpt['where'])) 
			foreach($tOpt['where'] as $k1 => $v1)
				//is_scalar:判断是否是整形、浮点型、字符串型 
				if(isset($this->field[$k1]) && is_scalar($v1)){
					# 整型格式化
					if(false !== strpos($this->field[$k1]['type'], 'int')){
						$tOpt['where'][$k1] = intval($v1);
					}
					# 浮点格式化
					elseif(false !== strpos($this->field[$k1]['type'], 'decimal')){
						$tOpt['where'][$k1] = floatval($v1);
					}
				}
		return $tOpt;
	}

	/**
	 * @desc 执行SQL
	 * @param string $pSql sql语句
	 */
	function exec($pSql){
		$this->db = &self::instance($this->_config);   //数据库连接
		if($tReturn = $this->db->exec($pSql)){
			$this->error = array();
		}
		else{
			$this->error = $this->db->errorInfo();  //errorInfo()是PDO的报错函数
			isset($this->error[1]) || $this->error = array();
		}
		return $tReturn;
	}

	/**
	 * @desc 开启本次查询缓存
	 * @param str $pKey MemKey 缓存的key
	 * @param int $pExpire 有效期
	 */
	private $cache = array();
	function cache($pKey = 'md5', $pExpire = 86400){
		$this->cache['key'] = $pKey;
		$this->cache['expire'] = $pExpire;
		return $this;
	}

	/**
	 * @desc 执行SQL，并返回结果
	 *@return array 结果数组
	 */
	function query(){
		$tArgs = func_get_args();   //获取所有参数
		$tSql = array_shift($tArgs);  //获取第一个参数
		// 使用缓存
		if($this->cache){
			$tMem = &Cache_Memcache::instance('session');
			if('md5' == $this->cache['key']){
				$this->cache['key'] = md5($tSql . ($tArgs? join(',', $tArgs): ''));
			}
			if(false !== ($tData = $tMem->get($this->cache['key']))){
				return $tData;
			}
		}
		// 查询数据库
		$this->db = &self::instance($this->_config);
		if($tArgs){
			//Prepares a statement for execution and returns a statement object.
			$tQuery = $this->db->prepare($tSql);
			//Executes a prepared statement.
			$tQuery->execute($tArgs);
		}
		else{
			$tQuery = $this->db->query($tSql);
		}
		if(!$tQuery) return array();
		// 不缓存查询结果
		if(!$this->cache){
			return $tQuery->fetchAll(PDO::FETCH_ASSOC);
		}
		// 缓存查询结果
		$tData = $tQuery->fetchAll(PDO::FETCH_ASSOC);
		$tMem->set($this->cache['key'], $tData, 0, $this->cache['expire']);
		$this->cache = array();
		return $tData;
	}

	/**
	 * @des 保存记录(自动区分 增/改)
	 *@param array $pData 要保存或添加的数据
	 *@return bool true|false
	 */
	function save($pData){
		return isset($pData[$this->pk])? $this->update($pData): $this->insert($pData);
	}

	/**
	 *@desc 添加记录
	 *@param array $pData 要插入的内容
	 *@return bool true|false
	 */
	function insert($pData, $pReplace = false){
		if($this->_filter($pData)){
			$tField = join(',', array_keys($pData));  //字段数组
			$tVal = join("','", $pData);
			$tSql = ($pReplace? "REPLACE": "INSERT") . " INTO $this->table($tField) VALUES ('$tVal')";
			if($this->exec($tSql)){
				return $this->db->lastInsertId();
			}
		}
		return false;
	}

	/**
	 * @desc 更新记录
	 *@param array $pData 要更新的内容
	 *@return bool true|false
	 */
	function update($pData){
		# 过滤
		if(!$this->_filter($pData)) return false;
		# 条件
		$tOpt = array();
		if(array_key_exists($this->pk, $pData)){
			$tOpt = array('where' => "$this->pk='{$pData[$this->pk]}'");
		}
		$tOpt = $this->_options($tOpt);
		# 更新
		if($pData && !empty($tOpt['where'])){
			foreach($pData as $k1 => $v1) $tSet[] = "$k1='$v1'";
			//if($this->exec("UPDATE " . $tOpt['table'] . " SET " . join(',', $tSet) . " WHERE " . $tOpt['where']))
			$this->exec("UPDATE " . $tOpt['table'] . " SET " . join(',', $tSet) . " WHERE " . $tOpt['where']);
			return true;
		}
		return false;
	}

	/**
	 * @desc 删除记录
	 *@return bool true|false
	 */
	function del(){
		if($tArgs = func_get_args()){
			# 主键删除
			$tSql = "DELETE FROM $this->table WHERE ";
			if(intval($tArgs[0]) || count($tArgs) > 1){
				$tSql.= $this->pk . ' IN(' . join(',', array_map("intval", $tArgs)) . ')';
				return $this->exec($tSql);
			}
			
			# 条件删除
			false === strpos($tArgs[0], '=') && exit('删除条件错误!');
			return $this->exec($tSql . $tArgs[0]);
		}
		# 连贯删除
		$tOpt = $this->_options();
		if(empty($tOpt['where'])) return false;
		$tSql = "DELETE FROM " . $tOpt['table'] . " WHERE " . $tOpt['where'];
		return $this->exec($tSql);
	}

	/**
	 * @des查找一条
	 *@param string|int $pId 查询的索引对象
	 */
	function fRow($pId = 0){
		$tOpt = $pId? $this->_options(array('where' => $this->pk . '=' . abs($pId))): $this->_options();
		$tOpt['where'] = empty($tOpt['where'])? '': ' WHERE ' . $tOpt['where'];
		$tOpt['order'] = empty($tOpt['order'])? '': ' ORDER BY ' . $tOpt['order'];
		//SQL出错时，会报出SQL语句，需要处理
		if($tResult = $this->query('SELECT ' . $tOpt['field'] . ' FROM ' . $tOpt['table'] . $tOpt['where'] . $tOpt['order'] . ' LIMIT 0,1')){
			return $tResult[0];
		}
		return array();
	}

	/**
	 * @desc查找一字段 ( 基于 fRow )
	 * @param string $pField 字段名
	 * @return string 字段值
	 */
	function fOne($pField){
		$this->field($pField);
		if(($tRow = $this->fRow()) && isset($tRow[$pField])){
			return $tRow[$pField];
		}
		return false;
	}

	/**
	 * @desc查找多条
	 *@param string|array $pOpt 查询条件或条件数组
	 *@return array 结果数组
	 */
	function fList($pOpt = array()){
		if(!is_array($pOpt)){
			$pOpt = array('where' => $this->pk . (strpos($pOpt, ',')? ' IN(' . $pOpt . ')': '=' . $pOpt));
		}
		$tOpt = $this->_options($pOpt);
		$tSql = 'SELECT ' . $tOpt['field'] . ' FROM ' . $tOpt['table'];
		$this->join && $tSql .= implode(' ', $this->join);
		empty($tOpt['where']) || $tSql .= ' WHERE ' . $tOpt['where'];
		empty($tOpt['group']) || $tSql .= ' GROUP BY ' . $tOpt['group'];
		empty($tOpt['order']) || $tSql .= ' ORDER BY ' . $tOpt['order'];
		empty($tOpt['having']) || $tSql.= ' HAVING '.$tOpt['having'];
		empty($tOpt['limit']) || $tSql .= ' LIMIT ' . $tOpt['limit'];
		return  $this->query($tSql);
	}

	/**
	 * @desc查询并处理为哈西数组 (基于fList)
	 * @param string $pField 查询字段
	 * @return array 查询结果数组
	 */
	function fHash($pField){
		$this->field($pField);
		$tList = array();
		$tField = explode(',', $pField);
		if(2 == count($tField)) 
			foreach($this->fList() as $v1) 
				$tList[$v1[$tField[0]]] = $v1[$tField[1]];
		else 
			foreach($this->fList() as $v1) 
				$tList[$v1[$tField[0]]] = $v1;
		return $tList;
	}

	/**
	 * @desc 获取数据库的所有数据表
	 * @return array
	 */
	function getTables(){
		$this->db = &self::instance($this->_config);
		return $this->db->query("SHOW TABLES")->fetchAll(3);
	}

	/**
	 * @desc获取数据表所有字段
	 * @return array
	 */
	function getFields($pTable){
		$this->db = &self::instance($this->_config);
		return $this->db->query("SHOW FULL FIELDS FROM " . $pTable)->fetchAll(2);
	}

	public $join = array();

	/**
	 * @desc组成复合连接查询条件
	 *@param array $pTable 数据表数组
	 *@param arr
	 */
	function join($pTable, $pWhere, $pPrefix = ''){
		$this->join[] = " $pPrefix JOIN $pTable ON $pWhere ";
		return $this;
	}

	/**
	 * @desc 密码加密
	 *@param int $salt 加密参数
	 *@param string $pwd 原始密码
	 *@return 返回加密后密码
	 */
	public function password($salt=null, $pwd){
		return md5($pwd.$salt);
	}

	/**
	 *@desc利用随机数生成加密参数
	 *@return int 加密参数
	 */
	public function salt(){
		return rand(1000,9999);
	}

	# # # # # # # # # # # / 事务 # # # # # # # # # # # /
	/**
	 * @desc 事务开始
	 */
	function begin(){
	}

	/**
	 * @desc事务提交
	 */
	function commit(){
	}

	/**
	 * 事务回滚
	 */
	function back(){
	}

}
