<?php
/**
 * @name IndexController
 * @author fudaoji
 * @desc 默认控制器
 */
class IndexController extends Ctrl_Base {

	/**
	 * @desc 初始化工作
	 */
	public function init(){
		parent::init();
		$this->auth();
		//Yaf_Dispatcher::getInstance()->disableView();
	}

	/** 
     * @desc网站首页动作函数
     */
	public function indexAction() {
		print_r($this->userInfo);exit;
	}

}
