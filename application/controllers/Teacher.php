<?php

/**
 * @name TeacherController
 * @author fudaoji
 * @desc 教师操作控制器
 */

class TeacherController extends Ctrl_Base{
	public $commonModel ;
	public $numberOfPage = 5; //每页显示条数
	public $uid;

	/**
	 * @desc初始化操作
	 */
	public function init(){
		parent::init();
		$this->uid = $_SESSION['uid'];
		$this->commonModel = new CommonModel();
	}

	/**
	 * @desc 显示个人信息 
	 */
	public function showPersonalInfoAction(){
		$teacherModel = new TeacherModel();
		$teacher = $teacherModel->getOneTeacher($this->uid);
		$sex  = $this->parameters['sex'];

		$this->assign(array('teacher'=>$teacher,
							'sex'=>$sex));
	}

	/**
	 * @desc 修改个人信息
	 */
	public function updatePersonalInfoAction(){
		$teacherModel = new TeacherModel();
		$teacher = $teacherModel->getOneTeacher($this->uid);
		$sex  = $this->parameters['sex'];

		$this->assign(array('teacher'=>$teacher,
							'sex'=>$sex));

		if ($uid = trim($this->post('uid'))) {
			$name     = trim($this->post('name'));
			$sex      = intval($this->post('sex'));
			$phone    = trim($this->post('phone'));
			$position = trim($this->post('position'));
			$email    = trim($this->post('email'));

			$form     = array('uid'=>$uid,'name'=>$name);
			$teacherInfo = array_merge($form, array('sex'=>$sex,'phone'=>$phone,
													'position'=>$position,
													'email'=>$email));

			//验证必填项信息
			if (!$this->commonModel->isNull($form)) {
				$this->ajax(false, $this->parameters['errMsg'][1]);
			}

			$result = $teacherModel->updateTeacher($teacherInfo);
			($result==12) && $this->ajax(false,$this->parameters['errMsg'][23],1);
			if($result==0){
				$this->ajax(true,$this->parameters['okMsg'][14]);
			}
		}
	}

	/**
	 * @desc 显示教授的所有课程
	 */
	public function showAllCourseAction(){
		//获取指定教师号的所有课程
		$courseModel = new CourseModel();
		$allCourse = $courseModel->getAllCoursesByTid($this->uid, $active=true);
		$fieldName = $courseModel->field;
		$active    = $this->parameters['active'];
		$term      = $this->parameters['term'];

		$this->assign(array('allCourse'=>$allCourse,
							'fieldName'=>$fieldName,
							'active'=>$active,
							'term'=>$term));
	}

	/**
	 *@desc显示指定课程的所有学生的成绩
	 *@param string $course_id 课程id
	 */
	public function showCourseGradesAction(){

		$course_id = $this->get('id');
		$courseModel = new CourseModel();
		$course   = $courseModel->getOneCourse($course_id);
		//获取某门课程的所有学生成绩
		$gradeModel = new GradeModel("course_id");
		$allGrades = $gradeModel->getAllGradeInfo($course_id);
		//获取选择某课程的所有学生信息
		$studentModel = new StudentModel('uid');
		$allStudent = $studentModel->getAllStudentInfo();

		//获取student表的所有字段名
		//$fieldName  = $this->gradeModel->field;
		$term = $this->parameters['term'];
		
		$this->assign(array('allGrades' => $allGrades,
							'term'  => $term,
							'allStudent' => $allStudent,
							'course'   => $course));
	}

	/**
	 * @desc 成绩编辑菜单
	 */
	public function inputGradesAction(){

		$course_id = $this->get('id');
		$courseModel = new CourseModel();
		$course   = $courseModel->getOneCourse($course_id);
		//获取某门课程的所有学生成绩
		$gradeModel = new GradeModel("course_id");
		$allGrades = $gradeModel->getAllGradeInfo($course_id);
		//获取选择某课程的所有学生信息
		$studentModel = new StudentModel('uid');
		$allStudent = $studentModel->getAllStudentInfo();

		//获取student表的所有字段名
		//$fieldName  = $this->gradeModel->field;
		$term = $this->parameters['term'];
		
		$this->assign(array('allGrades' => $allGrades,
							'term'  => $term,
							'allStudent' => $allStudent,
							'course'   => $course));
	}

	/**
	 * @desc 录入成绩
	 */
	public function saveGradesAction(){
		if ($_POST['submit']) {
			$course_id = $_POST['course_id'];
			$grades = $_POST['uid'];
			$gradeInfo = array();
			$gradeModel = new GradeModel();
			foreach ($grades as $uid => $grade) {
				$gradeInfo['student_id'] = $uid;
				$gradeInfo['course_id'] = $course_id;
				$gradeInfo['score'] = $grade; 
				$result = $gradeModel->saveGrade($gradeInfo);
				if ($result==24){
					header("Location: ".SITE_ROOT."teacher/error/msg/".$this->parameters['errMsg'][24]);exit();
				}

			}
			header("Location: ".SITE_ROOT."teacher/success/msg/".$this->parameters['okMsg'][15]);
		}
	}

	/**
	 * @desc 导出成绩单以excel格式保存
	 */
	public function downLoadGradeAction(){
		//获取指定课程的所有信息
		$course_id = $this->get('id');
		$courseModel = new CourseModel();
		$course   = $courseModel->getOneCourse($course_id);

		$term = $this->parameters['term'];
		$sheetTitle = $course['coursename']."成绩单"."-".$term[$course['term']];
		//获取指定课程的所有学生成绩
		$gradeModel = new GradeModel("course_id");
		$allGrades = $gradeModel->getAllGradeInfo($course_id);

		//获取所有学生信息以键值对的形式保存
		$studentModel = new StudentModel();
		$allStudent = $studentModel->getAllStudentInfo();

		date_default_timezone_set('Asia/Shanghai');

		if (PHP_SAPI == 'cli')
			die('请使用浏览器进行操作！');

		// 生成 PHPExcel 对象
		$objPHPExcel = new PHPExcel();

		//设置文件属性
		$objPHPExcel->getProperties()->setCreator("傅道集")
									 ->setTitle($sheetTitle)//文档名称
									 ->setSubject("成绩信息表")
									 ->setDescription("理学院".$sheetTitle."信息")
									 ->setKeywords("成绩单 信息");
		//添加列名称
		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A1', "学号")
		            ->setCellValue('B1', "姓名")
		            ->setCellValue('C1', "成绩")
		            ->setCellValue('D1', "名次");

		//添加具体信息
		$i = 2;
		foreach ($allGrades as $grade) {
			$objPHPExcel->setActiveSheetIndex(0) 
		            ->setCellValue('A'.$i, $grade['student_id'])
		            ->setCellValue('B'.$i, $allStudent[$grade['student_id']]['name'])
		            ->setCellValue('C'.$i, $grade['score'])
		            ->setCellValue('D'.$i, $i-1);
		    $i++;
		}

		//更改sheet名
		$objPHPExcel->getActiveSheet()->setTitle($sheetTitle);  

		//设置打开文件时显示的sheet
		$objPHPExcel->setActiveSheetIndex(0);  


		//设置文件类型
		header('Content-Type: application/vnd.ms-excel');  
		//设置默认的文件名
		header("Content-Disposition: attachment;filename=".$sheetTitle.".xls"); 
		header('Cache-Control: max-age=0');
		// 针对 IE 9
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit();
	}

	/**
	 * @desc 操作成功的提示信息
	 *@param string $msg 提示信息内容
	 */
	public function successAction($msg=''){
		$this->assign(array('msg' => $msg));
	}

	/**
	 * @desc 操作失败的提示信息
	 *@param string $msg 提示信息的内容
	 */
	public function errorAction($msg=''){
		$this->assign(array('msg' => $msg));
	}
}