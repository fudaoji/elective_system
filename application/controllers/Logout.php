<?php

/**
 * @name LogoutController
 * @author fudaoji
 * @desc 退出控制器
 */

class LogoutController extends Ctrl_Base {

	/**
	 * @desc 初始化工作
	 */
	public function init(){
		parent::init();
		Yaf_Dispatcher::getInstance()->disableView();
	}

	/**
	 * @desc 清除session保存的登录信息
	 */
	public function indexAction(){
		if (isset($_SESSION)) {
			session_destroy();
			header("Location: ".SITE_ROOT."login");
		}
	}
}