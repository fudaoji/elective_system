<?php
/**
 * @name ErrorController
 * @desc 错误控制器, 在发生未捕获的异常时刻被调用
 * @author fudaoji
 */
class ErrorController extends Ctrl_Base {

	/**
	 * @desc 初始化工作
	 */
	public function init(){
		parent::init();
		Yaf_Dispatcher::getInstance()->disableView();
	}

	/**
	*@desc 全局的错误提示函数
	*@param string $errmsg 提示的错误信息
	*/
	public function errorAction($errmsg=''){
		$errmsg = $errmsg ? $errmsg : "您访问的页面不存在！";
		$this->display('error',array('errmsg'=>$errmsg));
	}

	//从2.1开始, errorAction支持直接通过参数获取异常
	/*public function errorAction($exception) {
		//1. assign to view engine
		switch ($exception->getCode()) {
			case YAF_ERR_NOTFOUND_MODULE:
			case YAF_ERR_NOTFOUND_CONTROLLER:
			case YAF_ERR_NOTFOUND_ACTION:
			case YAF_ERR_NOTFOUND_VIEW:
				echo 404,":", $exception->getMessage();
				break;
			
			default:
				$message = $exception->getMessage();
				echo 0,":", $exception->getMessage();
				break;
		}
		$this->getView()->assign("exception", $exception);
		//5. render by Yaf 
	}*/
}
