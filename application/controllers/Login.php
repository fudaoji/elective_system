<?php

/**
 * @name LoginController
 * @author fudaoji
 * @desc 登录控制器
 */

class LoginController extends Ctrl_Base{
	public $loginModel;
	/**
	 * @desc 初始化工作
	 */
	public function init(){
		session_start();
		parent::init();
		$this->loginModel = new LoginModel();
	}


	/**
	 * @desc 默认登录动作
	 */
	public function loginAction(){
		$_SESSION['code'] && header("Location: ".SITE_ROOT);
		if ($code = trim($this->post('code'))) {
			$pwd   = trim($this->post('pwd'));
			$vcode  = trim($this->post('vcode'));
			$role  = intval(trim($this->post('role')));
			$info = array('code' => $code,  'pwd' => $pwd,
							   'vcode'=> $vcode, 'role'=> $role);

			/*验证表单数据是否为空*/
			$commonModel = new CommonModel();
			if (!$commonModel->isNull($info)) {
				$this->ajax(false, $this->parameters['errMsg'][1]);
			}
			if ($_SESSION['valicode']!=$vcode) {
				$this->ajax(false, $this->parameters['errMsg'][2], 2);
			}

			/*返回登录情况，登录成功的话返回0,返回其他数字都表示有错误*/
			$result=$this->loginModel->login($code, $pwd, $role);
			($result==3) && $this->ajax(false, $this->parameters['errMsg'][3], 3);
			($result==4) && $this->ajax(false, $this->parameters['errMsg'][4], 4);
			($result==5) && $this->ajax(false, $this->parameters['errMsg'][5], 5);
			
			$_SESSION['roleName'] = $this->parameters['role'][$role];
			($result==0) && $this->ajax(true);
			
		}
	}

	/**
	 * @desc 重置密码
	 */
	public function resetAction(){
		$_SESSION['code'] || header("Location: ".SITE_ROOT."login");
		if ($oldPwd = trim($this->post('oldPwd'))) {
			$newPwd     = trim($this->post('newPwd'));
			$confirmPwd = trim($this->post('confirmPwd'));

			$info = array('oldPwd'=>$oldPwd, 'newPwd'=>$newPwd, 'confirmPwd'=>$confirmPwd);
			/*验证表单数据是否为空*/
			$commonModel = new CommonModel();
			if (!$commonModel->isNull($info)) {
				$this->ajax(false, $this->parameters['errMsg'][1]);
			}

			//返回密码修改结果，修改成功的返回0,返回其他数字都表示有错误
			$info['code'] = $_SESSION['code'];
			$result = $this->loginModel->resetPassword($info);
			($result==6) && $this->ajax(false, $this->parameters['errMsg'][6], 6);
			($result==7) && $this->ajax(false, $this->parameters['errMsg'][7], 7);
			($result==0) && $this->ajax(true, $this->parameters['okMsg'][1]);
		}
	}

	/**
	 * @desc密码修改成功提示
	 */
	public function resetSuccessAction(){
		$_SESSION['code'] || header("Location: ".SITE_ROOT."login");
	}

	/**
	 * @desc生成验证码
	 *@return img 图片
	 */
	public function valicodeAction(){
		$commonModel = new CommonModel();
		return $commonModel->validateCode();
	}
}
