<?php

/**
 * @name StudentController
 * @author fudaoji
 * @desc 学生操作控制器
 */

class StudentController extends Ctrl_Base{
	public $commonModel ;
	public $numberOfPage = 5; //每页显示条数
	public $uid;

	/**
	 * @desc初始化操作
	 */
	public function init(){
		parent::init();
		$this->uid = $_SESSION['uid'];
		$this->commonModel = new CommonModel();
	}

	/**
	 * @desc 显示个人信息 
	 */
	public function showPersonalInfoAction(){
		$studentModel = new StudentModel();
		$student = $studentModel->getOneStudent($this->uid);
		$sex  = $this->parameters['sex'];
		$classModel   = new ClassModel('class_id');
		$majorModel   = new MajorModel('major_id');

		//获取所有班级信息
		$allClass     = $classModel->getAllClassInfo();
		//获取所有专业信息
		$allMajor     = $majorModel->getAllMajorInfo();

		$this->assign(array('student'=>$student,
							'allClass'=>$allClass,
							'allMajor'=>$allMajor,
							'sex'=>$sex));
	}

	/**
	 * @desc 修改个人信息
	 */
	public function updatePersonalInfoAction(){
		$studentModel = new StudentModel();
		$student = $studentModel->getOneStudent($this->uid);
		$sex  = $this->parameters['sex'];

		$this->assign(array('student'=>$student,
							'sex'=>$sex));

		if ($uid = trim($this->post('uid'))) {
			$name     = trim($this->post('name'));
			$sex      = intval($this->post('sex'));
			$phone    = trim($this->post('phone'));
			$position = trim($this->post('position'));
			$email    = trim($this->post('email'));

			$form     = array('uid'=>$uid,'name'=>$name);
			$studentInfo = array_merge($form, array('sex'=>$sex,'phone'=>$phone,
													'position'=>$position,
													'email'=>$email));

			//验证必填项信息
			if (!$this->commonModel->isNull($form)) {
				$this->ajax(false, $this->parameters['errMsg'][1]);
			}

			$result = $studentModel->updateStudent($studentInfo);
			($result==12) && $this->ajax(false,$this->parameters['errMsg'][23],1);
			if($result==0){
				$this->ajax(true,$this->parameters['okMsg'][14]);
			}
		}
	}

	/**
	 * @desc 显示可选的所有课程
	 */
	public function showAllCourseAction(){
		//获取专业号
		$studentModel = new StudentModel();
		$student = $studentModel->getOneStudent($this->uid);
		$major_id = $student['major_id'];
		//获取指定专业号的所有课程
		$courseModel = new CourseModel();
		$allCourse = $courseModel->getAllCoursesByMajorId($major_id, $active=true);
		$fieldName = $courseModel->field;
		$active    = $this->parameters['active'];
		$term      = $this->parameters['term'];

		$this->assign(array('allCourse'=>$allCourse,
							'fieldName'=>$fieldName,
							'term'=>$term));
	}

	/**
	 *@desc显示已选的课程
	 *@param string $course_id 课程id
	 */
	public function showSelectedCourseAction(){
		$uid = $this->uid;
		$gradeModel = new GradeModel("student_id");
		$grades = $gradeModel->getAllGradeByUid($uid);
		$allCourse = array();
		$courseModel = new CourseModel();
		foreach ($grades as $key => $grade) {
			$allCourse[] = $courseModel->getOneCourse($grade['course_id']);
		}
		$term = $this->parameters['term'];
		$fieldName = $courseModel->field;
		$this->assign(array('allCourse' => $allCourse,
							'term'  => $term,
							'fieldName' => $fieldName));
	}


	/**
	 * @desc选课
	 */
	public function selectCourseAction(){
		if ($_POST['submit']) {
			$uid = $this->uid; //学号
			$courses = $_POST['course'];
			$info = array('student_id'=>$uid);
			$gradeModel = new GradeModel();
			$courseModel = new CourseModel();
			foreach ($courses as $course_id) {
				$course = $courseModel->getOneCourse($course_id);
				if ($course['plimit']==$course['persons']) {
					header("Location: ".SITE_ROOT."student/error/msg/".$course['coursename'].$this->parameters['errMsg'][26]);exit();
				}
				$info['course_id'] = $course_id;
				$info['term'] = $course['term'];
				$result = $gradeModel->insertGrade($info);
				if ($result==25){
					header("Location: ".SITE_ROOT."student/error/msg/".$this->parameters['errMsg'][25]);exit();
				}elseif ($result==28) {
					header("Location: ".SITE_ROOT."student/error/msg/".$this->parameters['errMsg'][28]);exit();
				}elseif ($result==0) {
					$courseModel->addPersonNum($course_id);
				}

			}
			header("Location: ".SITE_ROOT."student/success/msg/".$this->parameters['okMsg'][16]);
		}
	}

	/**
	 * @desc 退课
	 */
	public function deleteCourseAction(){
		$course_id=trim($this->get('id'));
		$uid = $this->uid;

		$gradeModel = new GradeModel();
		$result = $gradeModel->deleteGrade($uid,$course_id);
		if ($result==0) {
			$courseModel = new CourseModel();
			$courseModel->reducePersonNum($course_id);
			header("Location: ".SITE_ROOT."student/success/msg/".$this->parameters['okMsg'][17]);exit();
		}
		header("Location: ".SITE_ROOT."student/error/msg/".$this->parameters['errMsg'][27]);exit();
	}


	/**
	 * @desc 操作成功的提示信息
	 *@param string $msg 提示信息内容
	 */
	public function successAction($msg=''){
		$this->assign(array('msg' => $msg));
	}

	/**
	 * @desc 操作失败的提示信息
	 *@param string $msg 提示信息的内容
	 */
	public function errorAction($msg=''){
		$this->assign(array('msg' => $msg));
	}
}