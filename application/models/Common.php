<?php
/**
 *@desc 公共操作Model
 *@author fudaoji
 */

class CommonModel{

	/**
     * @获取验证码
     * @return img
     */
	public function validateCode(){
		session_start();  //将验证码上的内容保存在session上
	 	for ($i=0; $i < 4; $i++) {
	 		/*生成随机数*/
	 		$char  = dechex(rand(1, 15));
			$rand .= $char;
			$text .= ' '.$char;
		}

		$_SESSION['valicode'] = $rand;

		//创建图片
		$width = 80; $height = 30;
		$img = imagecreatetruecolor($width, $height);

		//设置颜色
		$bgColor = imagecolorallocate($img, 0, 0, 0);
		$fontColor = imagecolorallocate($img, 255, 255, 255);

		//将随机数写入到图片中
		imagestring($img, 6, rand(2, 5), rand(2, 8), $text, $fontColor);

		//画干扰线
		/*for ($i = 0; $i < 2 ; $i++) {
			$randColor = imagecolorallocate($img, rand(10,255), rand(10,255), rand(10,255));
			imageline($img, 0,rand(0,30), 100, rand(0,30), $randColor);
		}*/

		//画噪点
		//imagesetpixel(图像句柄,x, y, color)
		for ($i = 0; $i < 100; $i++) {
			$randColor = imagecolorallocate($img, rand(10,255), rand(10,255), rand(10,255));
			imagesetpixel($img, rand()%100, rand()%30, $randColor);
		}

		//输出图像
		header("Content-Type: image/jpeg");
		imagejpeg($img);
	}

	/**
	 * @desc 检查参数是否为空
	 *@return true|false
	 */
	public function isNull($arr=array()){
		if (empty($arr)) {
			return false;
		}
		if (is_array($arr)) {
			foreach ($arr as $key => $value) {
				if(!$value) return false;			
			}
		}else{
			if (!$arr) return false;
		}
		return true;
	}

	/**
	 *@desc 分页框
	 *@param int $currentPage 当前页码
	 *@param int $totalPages 共有页数
	 *@param string $url 跳转url
	 */
	public function paginator($currentPage, $totalPages, $url){
		$html  = "<div id='example'></div><script type='text/javascript'>\n";
		$html .= "var options = {\n";
		$html .= "currentPage: %d,\n";
		$html .= "totalPages: %d,\n";
		$html .= "pageUrl: function(type, page, current){\n";
		$html .= "return '%s'+page;}}\n";
		$html .= "$('#example').bootstrapPaginator(options);</script>";
		$paginator  = sprintf($html, $currentPage,$totalPages,$url);

		return $paginator;
	}

}
