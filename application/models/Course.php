<?php

/**
 * @desc 课程信息操作Model
 *@author fudaoji
 */
class CourseModel extends Orm_Base{
	public $table = 'course';
	public $pk = 'course_id';
	public $field = array(
		'course_id' => array('type' => "char(10)", 'comment' => '课程号'),
		'coursename' => array('type' => "char(50)", 'comment' => '课程名称'),
		'course_description' => array('type' => "text", 'comment' => '课程描述'),
		'term' => array('type' => "int", 'comment' => '学期'),
		'credit' => array('type' => "float", 'comment' => '学分'),
		'hours' => array('type' => "int", 'comment' => '课时'),
		'plimit' => array('type' => "int", 'comment' => '限选人数'),
		'persons' => array('type' => "int", 'comment' => '已选人数'),
		'active' => array('type' => "int", 'comment' => '课程状态'),
		'tid' => array('type' => 'char', 'comment' => '任课教师'),
		'major_id' => array('type' => 'char', 'comment' => '所属专业')
	);

	/**
	 * @desc 获取所有课程列表
	 *@return array 
	 */
	public function getAllCourse($active=false){
		if($active==true) return $this->where("active=1")->fList();
		return $this->fList();
	}

	/**
	 * @desc 获取所有课程的信息
	 *@return array
	 */
	public function getAllCourseInfo($active=false){
		$allCourse     = $this->getAllCourse($active);
		$allCourseInfo = array();
		foreach ($allCourse as $course) {
			$allCourseInfo[$course['course_id']] = array('course_id'=>$course['course_id'],
														'coursename'=>$course['coursename'],
													  'course_description'=>$course['course_description'],
													  'term'=>$course['term'],'credit'=>$course['credit'],
													  'hours'=>$course['hours'],'plimit'=>$course['plimit'],
													  'persons'=>$course['persons'],'active'=>$course['active'],
													  'tid'=>$course['tid'],'major_id'=>$course['major_id']
													  );
		}
		return $allCourseInfo;
	}



	/**
	 * @desc获取指定课程号的一个课程
	 *@param array $course_id 指定的课程的id
	 *@return array 课程信息数组
	 */
	public function getOneCourse($course_id){
		return $this->fRow($course_id);
	}

	/**
	 * @desc添加一个课程
	 *@param array $info 要添加的课程的信息
	 *@return int 0|19
	 */
	public function insertCourse($info=array()){
		return ($this->insert($info) ? 0 : 19) ;
	}

	/**
	 * @desc删除一个课程
	 *@param string $course_id 要删除的课程的id
	 *@return 0|20
	 */
	public function deleteCourse($course_id){
		if($this->del($course_id)) return 0;
		return 20;
	}

	/**
	 * @desc更新课程信息
	 * @param array $info 课程信息
	 */
	public function updateCourse($info=array()){
		return ($this->update($info) ? 0 : 23);
	}

	/**
	 * @desc 根据教师号获取课程
	 *@return array 
	 */
	public function getAllCoursesByTid($tid, $active=false){
		if($active==true) return $this->where("tid=$tid and active=1")->fList();
		return $this->where("tid=$tid")->fList();
	}

	/**
	 * @desc 根据专业号获取课程
	 *@return array
	 */
	public function getAllCoursesByMajorId($major_id, $active=false){
		if($active==true) return $this->where("major_id='$major_id' and active=1")->fList();
		return $this->where("major_id='$major_id'")->fList();
	}

	/**
	 * @desc更新课程人数（增加）
	 *@param string $course_id 班级号
	 *@return bool true|false
	 */
	public function addPersonNum($course_id){
		$course = $this->fRow($course_id);
		return $this->update(array('course_id'=>$course_id, 'persons'=>$course['persons']+1));
	}

	/**
	 * @desc更新课程人数（减少）
	 *@param string $course_id 班级号
	 *@return bool true|false
	 */
	public function reducePersonNum($course_id){
		$course = $this->fRow($course_id);
		return $this->update(array('course_id'=>$course_id, 'persons'=>$course['persons']-1));
	}

}