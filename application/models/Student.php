<?php
/**
 * @desc 学生信息操作Model
 *@author fudaoji
 */

class StudentModel extends Orm_Base{
	public $table = 'student';
	public $pk = 'uid';
	public $field = array(
		'uid' => array('type' => "char(11)", 'comment' => '学号'),
		'name' => array('type' => "char(50)", 'comment' => '姓名'),
		'sex' => array('type' => "int(2)", 'comment' => '性别'),
		'major_id' => array('type' => "char(10)", 'comment' => '专业'),
		'class_id' => array('type' => "char(7)", 'comment' => "班级"),
		'position' => array('type' => "char(50)", 'comment' => "职位"),
		'phone' => array('type' => "char(11)", 'comment' => "联系电话"),
		'email' => array('type' => "char(50)", 'comment' => "电子邮箱"),
		'face_path' => array('type' => "char(100)", 'comment' => "头像")
	);

	/**
	 * @desc获取所有学生
	 *@return array
	 */
	public function getAllStudent(){
		return $this->fList();
	}

	/**
	 * @desc获取所有学生信息，以uid为键，学生信息数组为值
	 *@return array
	 */
	public function getAllStudentInfo(){
		$allStudent = $this->getAllStudent();
		$allStudentInfo = array();
		foreach ($allStudent as $student) {
			$allStudentInfo[$student['uid']] = array('uid'=>$student['uid'],
												 'name'=>$student['name']);
		}
		return $allStudentInfo;
	}

	/**
	 * @desc获取指定姓名的所有学生
	 *@return array
	 */
	public function getStudentsByName($searchKey){
		$this->name;
		$allStudent = array();
		$allStudent = $this->where("name like '%$searchKey%'")->fList();
		return $allStudent;
	}

	/**
	 * @desc获取 指定班级的所有学生
	 *@return array
	 */
	public function getStudentsByCid($class_id){
		$this->class_id;
		$allStudent = array();
		$allStudent = $this->where("class_id='$class_id'")->fList();
		return $allStudent;
	}

	/**
	 * @desc获取一个学生的信息
	 *@return array
	 */
	public function getOneStudent($uid){
		return $this->fRow($uid);
	}

	/**
	 * @desc添加一个学生
	 *@param array $info 要添加的学生的信息
	 *@return int 0|9
	 */
	public function insertStudent($info=array()){
		return ($this->insert($info) ? 0 : 9) ;
	}

	/**
	 * @desc更新学生数据
	 * @param array $userInfo 用户信息
	 */
	public function updateStudent($info=array()){
		return ($this->update($info) ? 0 : 12);
	}

	/**
	 * @desc删除一个学生
	 *@param string $uid 要删除的学生uid
	 *@return array
	 */
	public function deleteStudent($uid){
		if($this->del($uid)) return 0;
		return 10;
	}
}