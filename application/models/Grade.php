<?php

/**
 *@desc 课程信息操作Model
 *@author fudaoji
 */
class GradeModel extends Orm_Base{
	public $table = 'grade';
	public $pk = 'id';
	public $field = array(
		'id' => array('type' => "int(11)", 'comment' => '主键id'),
		'course_id' => array('type' => "char(10)", 'comment' => '课程号'),
		'student_id' => array('type' => "char(11)", 'comment' => '学生号'),
		'score'=>array('type'=>"float", "comment" => "成绩"),
		'term' =>array('type'=>"int(2)", "comment"=>"学期")
	);

	/**
	 * @desc获取一个课程的所有学生信息
	 *@return array
	 */
	public function getAllGradeInfo($course_id){
		return $this->where("course_id='$course_id'")->order("`score` DESC")->fList();
	}

	/**
	 * @desc获取一个课程的所有学生信息
	 *@return 0|24
	 */
	public function saveGrade($gradeInfo){
		$student_id = $gradeInfo['student_id'];
		$course_id  = $gradeInfo['course_id'];
		return ($this->where("student_id='$student_id' and course_id='$course_id'")->update($gradeInfo) ? 0 : 24);
	}

	/**
	 * @desc插入一条新的成绩信息
	 *@return 0|25
	 */
	public function insertGrade($gradeInfo){
		$student_id = $gradeInfo['student_id'];
		$course_id  = $gradeInfo['course_id'];
		if($this->where("student_id='$student_id' and course_id='$course_id'")->fRow())
			return 28;
		return ($this->insert($gradeInfo) ? 0 : 25);
	}

	/**
	 * @desc 根据student_id获取所有成绩
	 *@return array 成绩数组
	 */
	public function getAllGradeByUid($student_id){
		return $this->where("student_id='$student_id'")->order("`score` DESC")->fList();
	}

	/**
	 * @desc删除一条成绩信息
	 *@return 0|27
	 */
	public function deleteGrade($student_id,$course_id){
		return ($this->where("student_id='$student_id' and course_id='$course_id'")->del() ? 0 : 27);
	}

}