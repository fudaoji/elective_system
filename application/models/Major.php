<?php

/**
 * @desc 专业信息操作Model
 *@author fudaoji
 */
class MajorModel extends Orm_Base{
	public $table = 'major';
	public $pk = 'major_id';
	public $field = array(
		'major_id' => array('type' => "char(10)", 'comment' => '专业号'),
		'majorname' => array('type' => "char(50)", 'comment' => '专业名称'),
		'major_description' => array('type' => "text", 'comment' => '专业描述'),
	);

	/**
	 * @desc 获取所有专业列表
	 *@return array 
	 */
	public function getAllMajor(){
		return $this->fList();
	}

	/**
	 * @desc 获取所有专业的信息
	 *@return array
	 */
	public function getAllMajorInfo(){
		$allMajor     = $this->getAllMajor();
		$allMajorInfo = array();
		foreach ($allMajor as $major) {
			$allMajorInfo[$major['major_id']] = array('major_id' => $major['major_id'],
													  'majorname'=> $major['majorname'],
													  'major_description' => $major['major_description']);
		}
		return $allMajorInfo;
	}
}