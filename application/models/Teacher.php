<?php
/**
 * @desc 学生信息操作Model
 *@author fudaoji
 */

class TeacherModel extends Orm_Base{
	public $table = 'teacher';
	public $pk = 'uid';
	public $field = array(
		'uid' => array('type' => "char(11)", 'comment' => '教师号'),
		'name' => array('type' => "char(50)", 'comment' => '姓名'),
		'sex' => array('type' => "int(2)", 'comment' => '性别'),
		'position' => array('type' => "char(50)", 'comment' => "职位"),
		'phone' => array('type' => "char(11)", 'comment' => "联系电话"),
		'email' => array('type' => "char(50)", 'comment' => "电子邮箱"),
		'face_path' => array('type' => "char(100)", 'comment' => "头像")
	);

	/**
	 * @desc获取所有教师
	 *@return array
	 */
	public function getAllTeacher(){
		return $this->fList();
	}

	/**
	 * @desc获取指定姓名的所有教师
	 *@return array
	 */
	public function getTeachersByName($searchKey){
		$this->name;
		$allTeacher = array();
		$allTeacher = $this->where("name like '$searchKey'")->fList();
		return $allTeacher;
	}

	/**
	 * @desc获取一个教师信息
	 *@return array
	 */
	public function getOneTeacher($uid){
		return $this->fRow($uid);
	}

	/**
	 * @desc添加一个教师
	 *@param array $info 要添加的教师的信息
	 *@return int 0|9
	 */
	public function insertTeacher($info=array()){
		return ($this->insert($info) ? 0 : 9) ;
	}

	/**
	 * @desc更新教师信息
	 * @param array $userInfo 用户信息
	 */
	public function updateTeacher($info=array()){
		return ($this->update($info) ? 0 : 12);
	}

	/**
	 * @desc删除一个教师
	 *@param string $uid 要删除的教师uid
	 *@return array
	 */
	public function deleteTeacher($uid){
		if($this->del($uid)) return 0;
		return 10;
	}

	/**
	 * @desc获取所有教师的信息数组
	 *@return array
	 */
	public function getAllTeacherInfo(){
		$allTeacher = $this->getAllTeacher();
		$allTeacherInfo = array();
		foreach ($allTeacher as $teacher) {
			$allTeacherInfo[$teacher['uid']]=array('uid'=>$teacher['uid'],
												   'name'=>$teacher['name'],
												   'sex'=>$teacher['sex']);
		}
		return $allTeacherInfo;
	}

}