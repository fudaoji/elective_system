<?php

/**
 *@desc 班级信息操作Model
 *@author fudaoji
 */
class ClassModel extends Orm_Base{
	public $table = 'class';
	public $pk = 'class_id';
	public $field = array(
		'class_id' => array('type' => "char(7)", 'comment' => '班级号'),
		'classname' => array('type' => "char(50)", 'comment' => '班级名称'),
		'persons' => array('type' => "int(5)", 'comment' => '班级人数'),
	);

	/**
	 * @desc获取一个班级的信息
	 *@return array
	 */
	public function getOneClass($class_id){
		return $this->fRow($class_id);
	}

	/**
	 * @desc获取所有班级
	 *@return array
	 */
	public function getAllClass(){
		return $this->fList();
	}

	/**
	 * @desc获取所有班级信息
	 *@return array
	 */
	public function getAllClassInfo(){
		$allClass     = $this->getAllClass();
		$allClassInfo = array();
		foreach ($allClass as $class) {
			$allClassInfo[$class['class_id']] = array('class_id' => $class['class_id'],
													  'classname'=> $class['classname'],
													  'persons'  => $class['persons']);
		}
		return $allClassInfo;
	}

	/**
	 * @desc更新班级人数（增加）
	 *@param string $class_id 班级号
	 *@return bool true|false
	 */
	public function addPersonNum($class_id){
		$class = $this->fRow($class_id);
		return $this->update(array('class_id'=>$class_id, 'persons'=>$class['persons']+1));
	}

	/**
	 * @desc更新班级人数（减少）
	 *@param string $class_id 班级号
	 *@return bool true|false
	 */
	public function reducePersonNum($class_id){
		$class = $this->fRow($class_id);
		if ($class['persons']>=1) {
			return $this->update(array('class_id'=>$class_id, 'persons'=>$class['persons']-1));
		}
		return false;
	}

	/**
	 * @desc添加一个班级
	 *@param array $info 要添加的班级的信息
	 *@return int 0|14
	 */
	public function insertClass($info=array()){
		return ($this->insert($info) ? 0 : 14) ;
	}

	/**
	 * @desc删除一个班级
	 *@param string $uid 要删除的班级的id
	 *@return 0|15
	 */
	public function deleteClass($class_id){
		if($this->del($class_id)) return 0;
		return 15;
	}

	/**
	 * @desc更新班级信息
	 * @param array $info 班级信息
	 */
	public function updateClass($info=array()){
		return ($this->update($info) ? 0 : 17);
	}
}