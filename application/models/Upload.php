<?php
/**
 *@desc 上传文件操作Model
 *@author fudaoji
 */

class UploadModel{
	/**
	 * @desc生成上传文件的保存目录
	 *@param string $upload_dir 目录
	 *@param string $errUrl 错误页面路径
	 */
	public function createUploadsDir($upload_dir, $errUrl){
		$dirArr     = explode('/',$upload_dir);
		$uploadsDir = '';
		$depth      = count($dirArr);
		//确定存放文件的文件夹存在
		for ($i=0; $i < $depth; $i++) {
			$uploadsDir .= $dirArr[$i]."/";
			if (!is_dir(WWW_ROOT . $uploadsDir)) {
				@mkdir(WWW_ROOT . $uploadsDir, 0777) or 
					header("Location: ".SITE_ROOT.$errUrl."msg/新建文件夹".$uploadsDir."失败");exit();
			}
		}
		//确定存放文件的文件夹存在
		if (!is_dir($upload_dir)) {
			@mkdir($upload_dir, 0777) or 
				header("Location: ".SITE_ROOT.$errUrl."msg/新建文件夹失败");exit();
		}
	}

	/**
	 * @desc判断上传文件出现的错误
	 *@param string $fieldFileName 文件表单的name值
	 */
	public function getErrors($fileFieldName,$fileExt=array(),$errUrl){

		//错误信息类型
		$php_errors = array(1 => '文件过大，请重新上传',
							2 => '文件过大，请重新上传',
							3 => '文件不能完整上传',
							4 => '没有文件被选中');
		//允许上传的文件后缀名
		//$fileExt = array("xls", "xlsx");

		//确定文件是否上传成功
		($_POST[$fileFieldName]['error']==0) or
			header("Location: ".SITE_ROOT.$errUrl."msg/".
				$php_errors[$_FILES[$fileFieldName]['error']]); exit();

		//判断是否是上传的文件
		@is_uploaded_file($_FILES[$fileFieldName]['tmp_name']) or 
			header("Location: ".SITE_ROOT.$errUrl."msg/上传错误文件".$_FILES[$fileFieldName]['tmp_name']); exit();

		//判断文件类型
		if(!in_array(strtolower(substr(strrchr($_FILES[$fileFieldName]['name'], '.'), 1)),$fileExt)){   
		    $text=implode(",",$fileExt);
		    header("Location: ".SITE_ROOT.$errUrl."msg/您只能上传以下类型文件: ".$text);exit();
		}    
	}

	/**
	 *@desc上传文件的重命名
	 *@param string $upload_dir 文件保存目录
	 *@param string $fileFieldName 文件表单的name值
	 */
	public function renameFile($upload_dir, $fileFieldName){
		$now = time();
		while(file_exists($fileName=$upload_dir.'/'.$now.'-'.$_FILES[$fileFieldName]['name'])) {
			$now++;
		}
		return $fileName;
	}
}
?>