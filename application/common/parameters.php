<?php
/**
 * @desc公共信息
 *@author fudaoji
 */

//课程状态
$active = array(2 => "关闭", 1 => "开启");

//性别
$sex = array(0 => '男', 1 => '女');

//学期
$term = array(1 => '上学期', 2 => '下学期');

//身份
$role = array(1 => '管理员', 2 => '教师', 3 => '学生'); 

//错误提示
$errMsg = array(1 => '请确认信息是否完整填写',
				2 => '验证码输入错误',
				3 => '帐号不存在',
				4 => '密码错误',
				5 => '身份类型选择错误',
				6 => '旧密码错误',
				7 => '两次输入的密码不一致',
				8 => '该用户已存在',
				9 => '用户添加失败，请重试',
				10 => '删除用户失败，请重试',
				11 => '文件上传失败，请重试',
				12 => '修改用户信息失败',
				13 => '请输入搜索关键词',
				14 => '班级添加失败',
				15 => '删除班级失败',
				16 => '此班级有学生存在，无法删除',
				17 => '修改班级信息失败',
				18 => '修改用户密码失败',
				19 => '课程添加失败',
				20 => '删除课程失败',
				21 => '当前课程为开启状态，请关闭课程后再删除',
				22 => '修改课程信息失败',
				23 => '个人信息保存失败',
				24 => '成绩录入失败',
				25 => '选课失败',
				26 => '课程已选满',
				27 => '退课失败',
				28 => '你已选过该课程');

//操作成功提示
$okMsg = array(1 => '密码修改成功',
			   2 => '添加学生成功',
			   3 => '删除学生成功',
			   4 => '文件上传成功',
			   5 => '数据导入成功',
			   6 => '修改用户信息成功',
			   7 => '添加教师成功',
			   8 => '添加班级成功',
			   9 => '删除班级成功',
			   10=> '修改班级信息成功',
			   11=> '添加课程成功',
			   12=> '删除课程成功',
			   13=> '修改课程信息成功',
			   14=> '个人信息保存成功',
			   15=> '成绩录入成功',
			   16 => '选课成功',
			   17 => '退课成功');

$parameters = array('sex'   => $sex, 
					'term'  => $term,
					'role'  => $role,
					'errMsg'=> $errMsg,
					'okMsg' => $okMsg,
					'active'=> $active);
?>