<?php
/**
 * @name Bootstrap
 * @author fudaoji
 * @desc 所有在Bootstrap类中, 以_init开头的方法, 都会被Yaf调用,
 * 这些方法, 都接受一个参数:Yaf_Dispatcher $dispatcher
 * 调用的次序, 和申明的次序相同
 */
class Bootstrap extends Yaf_Bootstrap_Abstract{

    public function _initConfig() {
		//把配置保存起来
		$arrConfig = Yaf_Application::app()->getConfig();
		Yaf_Registry::set('config', $arrConfig);
		//定义常量：网站根目录
		define('PATH_APP', Yaf_Registry::get("config")->application->directory); 
		//定义常量：网站域名
		define('SITE_ROOT', '/index.php/');
		//定义常量：视图模板目录
		define('PATH_TPL', PATH_APP . '/views');
		//定义常量：项目代码所在根目录
		/*define('WWW_ROOT', '/var/www/elective_system/');*/
		define('WWW_ROOT', 'D:/prog_tools/wamp/www/elective_system/');
	}

	public function _initPlugin(Yaf_Dispatcher $dispatcher) {
		//注册一个插件
		$objSamplePlugin = new SamplePlugin();
		$dispatcher->registerPlugin($objSamplePlugin);
	}

	public function _initRoute(Yaf_Dispatcher $dispatcher) {
		//在这里注册自己的路由协议,默认使用简单路由
		$route_config = new Yaf_Config_Ini(APPLICATION_PATH . "/conf/route.ini");
		$route = Yaf_Dispatcher::getInstance()->getRouter();
		$route->addConfig($route_config->routes);
	}
	
	public function _initView(Yaf_Dispatcher $dispatcher){
		//在这里注册自己的view控制器，例如smarty,firekylin
	}
}
