-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 09 月 12 日 19:47
-- 服务器版本: 5.5.32
-- PHP 版本: 5.3.10-1ubuntu3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `xuanke`
--

-- --------------------------------------------------------

--
-- 表的结构 `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `uid` varchar(11) NOT NULL COMMENT '帐号',
  `mid` tinyint(2) NOT NULL DEFAULT '2' COMMENT '权限值',
  `salt` int(4) NOT NULL COMMENT '加密参数',
  `password` char(32) NOT NULL COMMENT '用户密码',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- --------------------------------------------------------

--
-- 表的结构 `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `class_id` char(7) NOT NULL COMMENT '班级号',
  `classname` varchar(50) NOT NULL COMMENT '班级名',
  `persons` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='班级信息';

-- --------------------------------------------------------

--
-- 表的结构 `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `course_id` varchar(10) NOT NULL COMMENT '课程号',
  `coursename` varchar(50) NOT NULL COMMENT '课程名',
  `course_description` text COMMENT '课程描述',
  `term` tinyint(2) NOT NULL DEFAULT '1' COMMENT '开课学期',
  `credit` float NOT NULL COMMENT '学分',
  `hours` int(5) NOT NULL COMMENT '学时',
  `plimit` int(5) NOT NULL COMMENT '限选人数',
  `persons` int(5) NOT NULL DEFAULT '0' COMMENT '已选人数',
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '开课状态(是/否)',
  `tid` varchar(11) DEFAULT NULL COMMENT '任课教师号',
  `major_id` varchar(11) NOT NULL COMMENT '所属专业',
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程信息';

-- --------------------------------------------------------

--
-- 表的结构 `grade`
--

CREATE TABLE IF NOT EXISTS `grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分数id',
  `course_id` varchar(10) NOT NULL COMMENT '课程号',
  `student_id` varchar(11) NOT NULL COMMENT '学生号',
  `score` float NOT NULL DEFAULT '0' COMMENT '分数',
  `term` tinyint(2) NOT NULL DEFAULT '1' COMMENT '学期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='分数信息' AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- 表的结构 `major`
--

CREATE TABLE IF NOT EXISTS `major` (
  `major_id` varchar(10) NOT NULL COMMENT '专业号',
  `majorname` varchar(50) NOT NULL COMMENT '专业名称',
  `major_description` text NOT NULL COMMENT '专业描述',
  PRIMARY KEY (`major_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='专业信息';

-- --------------------------------------------------------

--
-- 表的结构 `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `uid` varchar(11) NOT NULL COMMENT '学号',
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `sex` tinyint(2) DEFAULT '0' COMMENT '性别',
  `major_id` varchar(10) NOT NULL COMMENT '专业',
  `class_id` char(7) NOT NULL COMMENT '所在班级',
  `position` varchar(50) DEFAULT NULL COMMENT '职位',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '电子邮件',
  `face_path` varchar(100) DEFAULT NULL COMMENT '头像保存路径',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学生信息表';

-- --------------------------------------------------------

--
-- 表的结构 `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `uid` varchar(11) NOT NULL COMMENT '教师号',
  `name` varchar(50) NOT NULL COMMENT '教师姓名',
  `sex` tinyint(2) DEFAULT '0' COMMENT '性别',
  `position` varchar(50) DEFAULT NULL COMMENT '职称',
  `phone` varchar(11) DEFAULT NULL COMMENT '电话',
  `email` varchar(50) DEFAULT NULL COMMENT '电子邮箱',
  `face_path` varchar(100) DEFAULT NULL COMMENT '头像保存路径',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='教师信息';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
