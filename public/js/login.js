$(function() {
	$('#inputUser').focus();
	$('#submit').click(function() {
		var code  = $('#inputUser').val();
		var pwd   = $('#inputPassword').val();
		var vcode = $('#vcode').val();
		var role  = $('#role').val();

		if (code == '') {
			$('.errMsgUid').css('display','block').html('请輸入帐号');
			$('#inputUser').focus(function(){
				$('.errMsgUid').css('display','none');
			});
			return false;
		}
		
		if (pwd == '') {
			$('.errMsgPwd').css('display','block').html('请输入密碼!');
			$('#inputPassword').focus(function(){
				$('.errMsgPwd').css('display','none');
			});
			return false;
		}
		if (vcode.length != 4) {
			$('.errMsgCode').css('display','block').html('请输入正确的验证码');
			$('#vcode').val('');
			$('#vcode').focus(function(){
				$('.errMsgCode').css('display','none');
			});
			return false;
		};
		$.post('/index.php/login', {code:code,pwd:pwd,vcode:vcode,role:role}, function(data) {
			if (data.status == true) {
				location.href = '/index.php';
			} else {
				switch(data.data){
					case 1:
						$('.commonMsg').css('display','block').html(data.info);
						return false;break;
					case 2:
						$('.errMsgCode').css('display','block').html(data.info);
						$('#vcode').val('');
						$('#vcode').focus(function(){
							$('.errMsgCode').css('display','none');
						});
						return false;break;
					case 3:
						$('.errMsgUid').css('display','block').html(data.info);
						$('#inputUser').val('');
						$('#inputUser').focus(function(){
							$('.errMsgUid').css('display','none');
						});
						return false;break;
					case 4:
						$('.errMsgPwd').css('display','block').html(data.info);
						$('#inputPassword').val('');
						$('#inputPassword').focus(function(){
							$('.errMsgPwd').css('display','none');
						});
						return false;break;
					case 5:
						$('.errMsgRole').css('display','block').html(data.info);
						$('#role').click(function(){
							$('.errMsgRole').css('display','none');
						})
						return false;break;
				}
			}
		}, 'json');
	});
});