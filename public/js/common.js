function showDiv(obj) {
	obj.style.display = "block";
}

function hideDiv(obj) {
	obj.style.display = "none";
}

	window.onscroll = function() {
		var oDiv = document.getElementById('to-top');
		var b = document.documentElement.scrollTop == 0 ? document.body.scrollTop : document.documentElement.scrollTop;
		var c = document.documentElement.scrollTop == 0 ? document.body.scrollHeight : document.documentElement.scrollHeight;

		if (b > c / 5) {
			showDiv(oDiv);
		} else {
			hideDiv(oDiv);
		};

	}

$(function(){
	$("body").keydown(function() {
	    if (event.keyCode == "13") {//keyCode=13是回车键
	        $('#submit').click();
	    }
	});
	$(".nav-header").removeClass('in');

	$("tr:even").addClass("success");

})
