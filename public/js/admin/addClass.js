$(function() {
        $("#class-menu").addClass('in');
        $('#cid').focus();
        $('#submit').click(function() {
          var cid           = $('#cid').val();
          var classname     = $('#classname').val();
          var persons       = $('#persons').val();

          if (cid == '') {
              $('.errMsgCid').css('display','block').html('请輸入班级号!');
              $('#cid').focus(function(){
                $('.errMsgCid').css('display','none');
              });
              return false;
          }

          if (classname == '') {
              $('.errMsgName').css('display','block').html('请输入班级名称!');
              $('#classname').focus(function(){
                $('.errMsgName').css('display','none');
              });
              return false;
          }

          if (persons == '') {
              $('.errMsgPersons').css('display','block').html('请輸入班级人数!');
              $('#cid').focus(function(){
                $('.errMsgPersons').css('display','none');
              });
              return false;
          }
          
          $.post('/index.php/admin/add-class/', {cid:cid,classname:classname,persons:persons}, function(data) {
              if (data.status == true) {
                location.href = '/index.php/admin/success/msg/'+data.info;
              } else {
                  switch(data.data){
                      case 1:
                        $('.commonMsg').css('display','block').html(data.info);
                        return false;break;
                      case 14:
                        location.href = '/index.php/admin/error/msg/'+data.info;
                        return false;break;
                  }
              }
          }, 'json');
        });
    });