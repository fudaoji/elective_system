$(function() {
        $("#student-menu").addClass('in');
        $('#uid').focus();
        $('#submit').click(function() {
          var uid      = $('#uid').val();
          var name     = $('#name').val();
          var pwd      = $('#pwd').val();
          var sex      = $('#sex').val();
          var major_id = $('#major_id').val();
          var class_id = $('#class_id').val();
          var phone    = $('#phone').val();


          if (uid == '') {
              $('.errMsgUid').css('display','block').html('请輸入学号!');
              $('#uid').focus(function(){
                $('.errMsgUid').css('display','none');
              });
              return false;
          }

          if (pwd == '') {
              $('.errMsgPwd').css('display','block').html('请输入密碼!');
              $('#pwd').focus(function(){
                $('.errMsgPwd').css('display','none');
              });
              return false;
          }

          if (name == '') {
              $('.errMsgName').css('display','block').html('请输入姓名!');
              $('#name').focus(function(){
                $('.errMsgName').css('display','none');
              });
              return false;
          }
          
          $.post('/index.php/admin/add-student', {uid:uid,pwd:pwd,name:name,sex:sex,major_id:major_id,class_id:class_id,phone:phone}, function(data) {
              if (data.status == true) {
                location.href = '/index.php/admin/success/msg/'+data.info;
              } else {
                  switch(data.data){
                      case 1:
                        $('.commonMsg').css('display','block').html(data.info);
                        return false;break;
                      case 8:
                        $('.errMsgUid').css('display','block').html(data.info);
                        $('#uid').val('');
                        $('#uid').focus(function(){
                          $('.errMsgUid').css('display','none');
                        });
                        return false;break;
                  }
              }
          }, 'json');
        });
    });