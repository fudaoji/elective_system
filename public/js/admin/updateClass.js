$(function() {
        $("#class-menu").addClass('in');
        $('#cid').focus();
        $('#submit').click(function() {
          var class_id  = $('#cid').val();
          var classname = $('#classname').val();
          var persons   = $('#persons').val();

          if (class_id == '') {
              $('.errMsgCid').css('display','block').html('请輸入班级号!');
              $('#cid').focus(function(){
                $('.errMsgCid').css('display','none');
              });
              return false;
          }

          if (classname == '') {
              $('.errMsgName').css('display','block').html('请输入班级名称!');
              $('#name').focus(function(){
                $('.errMsgName').css('display','none');
              });
              return false;
          }
          
          $.post('/index.php/admin/update-class/', {class_id:class_id,classname:classname,persons:persons}, function(data) {
              if (data.status == true) {
                location.href = '/index.php/admin/success/msg/'+data.info;
              } else {
                  switch(data.data){
                      case 1:
                        $('.commonMsg').css('display','block').html(data.info);
                        return false;break;
                  }
              }
          }, 'json');
        });
    });