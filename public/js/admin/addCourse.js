$(function() {
        $("#course-menu").addClass('in');
        $('#courseId').focus();
        $('#submit').click(function() {
          var courseId   = $('#courseId').val();
          var courseName = $('#courseName').val();
          var description= $('#description').val();
          var term       = $('#term').val();
          var credit     = $('#credit').val();
          var hours      = $('#hours').val();
          var plimit     = $('#plimit').val();
          var active     = $('#active').val();
          var tid        = $('#tid').val();
          var major_id   = $('#major').val();

          if (courseId == '') {
              $('.errMsgCourseId').css('display','block').html('请輸入课程号!');
              $('#courseId').focus(function(){
                $('.errMsgCourseId').css('display','none');
              });
              return false;
          }

          if (courseName == '') {
              $('.errMsgCourseName').css('display','block').html('请输入课程名称!');
              $('#courseName').focus(function(){
                $('.errMsgCourseName').css('display','none');
              });
              return false;
          }

          if (credit == '') {
              $('.errMsgCredit').css('display','block').html('请輸入课程学分!');
              $('#credit').focus(function(){
                $('.errMsgCredit').css('display','none');
              });
              return false;
          }

          if (hours == '') {
              $('.errMsgHours').css('display','block').html('请輸入课程课时!');
              $('#hours').focus(function(){
                $('.errMsgHours').css('display','none');
              });
              return false;
          }

          if (plimit == '') {
              $('.errMsgPlimit').css('display','block').html('请輸入选课人数限制!');
              $('#plimit').focus(function(){
                $('.errMsgCredit').css('display','none');
              });
              return false;
          }
          
          $.post('/index.php/admin/add-course/', {courseId:courseId,courseName:courseName,description:description,term:term,credit:credit,hours:hours,plimit:plimit,active:active,tid:tid,major_id:major_id}, function(data) {
              if (data.status == true) {
                  location.href = '/index.php/admin/success/msg/'+data.info;
              } else {
                  switch(data.data){
                      case 1:
                        $('.commonMsg').css('display','block').html(data.info);
                        return false;break;
                      case 19:
                        location.href = '/index.php/admin/error/msg/'+data.info;
                        return false;break;
                  }
              }
          }, 'json');
        });
    });