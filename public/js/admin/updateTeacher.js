$(function() {
        $("#teacher-menu").addClass('in');
        $('#uid').focus();
        $('#submit').click(function() {
          var uid      = $('#uid').val();
          var name     = $('#name').val();
          var sex      = $('#sex').val();
          var position = $('#position').val();
          var email    = $('#email').val();
          var phone    = $('#phone').val();


          if (uid == '') {
              $('.errMsgUid').css('display','block').html('请輸入学号!');
              $('#uid').focus(function(){
                $('.errMsgUid').css('display','none');
              });
              return false;
          }

          if (name == '') {
              $('.errMsgName').css('display','block').html('请输入姓名!');
              $('#name').focus(function(){
                $('.errMsgName').css('display','none');
              });
              return false;
          }
          
          $.post('/index.php/admin/update-teacher/', {uid:uid,name:name,sex:sex,position:position,email:email,phone:phone}, function(data) {
              if (data.status == true) {
                var successMsg = "教师信息保存成功";
                location.href = '/index.php/admin/success/msg/'+successMsg;
              } else {
                  switch(data.data){
                      case 1:
                        $('.commonMsg').css('display','block').html(data.info);
                        return false;break;
                  }
              }
          }, 'json');
        });
    });