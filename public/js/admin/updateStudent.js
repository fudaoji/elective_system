$(function() {
        $("#student-menu").addClass('in');
        $('#uid').focus();
        $('#submit').click(function() {
          var oldUid  = $('#oldUid').val();
          var uid      = $('#uid').val();
          var name     = $('#name').val();
          var pwd      = $('#pwd').val();
          var sex      = $('#sex').val();
          var position = $('#position').val();
          var major_id = $('#major_id').val();
          var class_id = $('#class_id').val();
          var email    = $('#email').val();
          var phone    = $('#phone').val();


          if (uid == '') {
              $('.errMsgUid').css('display','block').html('请輸入学号!');
              $('#uid').focus(function(){
                $('.errMsgUid').css('display','none');
              });
              return false;
          }

          if (name == '') {
              $('.errMsgName').css('display','block').html('请输入姓名!');
              $('#name').focus(function(){
                $('.errMsgName').css('display','none');
              });
              return false;
          }
          
          $.post('/index.php/admin/update-student/', {oldUid:oldUid,uid:uid,name:name,sex:sex,position:position,major_id:major_id,class_id:class_id,email:email,phone:phone}, function(data) {
              if (data.status == true) {
                var successMsg = "学生信息保存成功";
                location.href = '/index.php/admin/success/msg/'+successMsg;
              } else {
                  switch(data.data){
                      case 1:
                        $('.commonMsg').css('display','block').html(data.info);
                        return false;break;
                  }
              }
          }, 'json');
        });
    });